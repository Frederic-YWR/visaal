import { Component } from '@angular/core';
import { PatientsService} from 'src/app/shared/patients.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { from , throwError } from 'rxjs';
import { map,switchMap,catchError, tap} from 'rxjs/operators';

@Component({
  selector: 'app-blank-page',
  templateUrl: './blank-page.component.html'
})
export class BlankPageComponent {

  data:any[];
  constructor(
      private patientsService: PatientsService,
        private afs :AngularFirestore
  ) {
  
  }
  
  ngOnInit(): void {
   
   this.patientsService.getAllPatients$().pipe(
   tap( (patients) => this.data = patients),
   tap((list)=> console.log(list))
   )
   .subscribe();
  
  }
  
   


}
