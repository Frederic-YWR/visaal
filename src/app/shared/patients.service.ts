import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { from , throwError,  Observable} from 'rxjs';
import { map,switchMap,catchError, tap} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  constructor(
    private afs :AngularFirestore,
    
  ) {
  }
  
  getAllPatients$(): Observable<any>  {
  
     return this.afs.collection('patients')
                .valueChanges();
  }
  
 
}
